import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';


interface IModelOfFighter {
  _id: string,
  name: string,
  health: number,
  attack: number,
  defense: number,
  source: string
}

export function showWinnerModal(fighter: IModelOfFighter) {
  // call showModal function
  showModal({
    title: `Today winner is ${fighter.name}`,
    bodyElement: fighter,
    onClose: () => {
      location.reload();
    },
  });
}
