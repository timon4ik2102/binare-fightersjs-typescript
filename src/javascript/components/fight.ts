import { controls } from '../../constants/controls';

interface IFighter {
  name: string,
  attack: number,
  defense: number,
  startHealth: number,
  criticalHitTime: number,
  health: number
 }

interface IModelOfFighter {
  _id: string,
  name: string,
  health: number,
  attack: number,
  defense: number,
  source: string
}

export async function fight(firstFighter: IModelOfFighter, secondFighter: IModelOfFighter) {
  return new Promise((resolve) => {
    let pressedKeys = new Set();
    let curTime = new Date().valueOf();

    const leftFighter = Object.assign(firstFighter, {
      startHealth: firstFighter.health,
      criticalHitTime: curTime - 10001,
    });
    const rightFighter = Object.assign(secondFighter, {
      startHealth: secondFighter.health,
      criticalHitTime: curTime - 10001,
    });

    document.addEventListener('keydown', (event) => {
      pressedKeys.add(event.code);
      if (
        event.code === controls.PlayerOneAttack &&
        !pressedKeys.has(controls.PlayerTwoBlock) &&
        !pressedKeys.has(controls.PlayerOneBlock)
      ) {
        rightFighter.health -= getDamage(leftFighter, rightFighter);
        console.log(rightFighter.health);
        percentHealthIndicator(rightFighter, 'right');
      }

      if (
        event.code === controls.PlayerTwoAttack &&
        !pressedKeys.has(controls.PlayerOneBlock) &&
        !pressedKeys.has(controls.PlayerTwoBlock)
      ) {
        leftFighter.health -= getDamage(rightFighter, leftFighter);
        percentHealthIndicator(leftFighter, 'left');
      }

      if (controls.PlayerOneCriticalHitCombination.every((key) => pressedKeys.has(key))) {
        doneCriticalDamage(leftFighter, rightFighter);
        percentHealthIndicator(rightFighter, 'right');
      }

      if (controls.PlayerTwoCriticalHitCombination.every((key) => pressedKeys.has(key))) {
        doneCriticalDamage(rightFighter, leftFighter);
        percentHealthIndicator(leftFighter, 'left');
      }
    });

    document.addEventListener('keyup', (event) => {
      pressedKeys.delete(event.code);
      if (rightFighter.health <= 0) {
        resolve(firstFighter);
      }
      if (leftFighter.health <= 0) {
        resolve(secondFighter);
      }
    });
  });
}

export function getDamage(attacker: IFighter, defender: IFighter) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage > 0) return damage;
  else return 0;
}

export function getHitPower(fighter: IFighter) {
  // return hit power
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter: IFighter) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}

export function getCriticalHit(fighter: IFighter) {
  // return critical damage
  return fighter.attack * 2;
}

export function doneCriticalDamage(attacker: IFighter, defender: IFighter) {
    const currentTime = new Date().valueOf();
  const timeLimit = 10000;
  if (currentTime - attacker.criticalHitTime > timeLimit) {
    defender.health -= getCriticalHit(attacker);
    attacker.criticalHitTime = currentTime;
  }
}

export function percentHealthIndicator(defender: IFighter, side: string) {
  if (defender.health >= 0) {
    document.getElementById(`${side}-fighter-indicator`)!.style.width = `${Math.round((defender.health / defender.startHealth) * 100)}%`;
  }
}