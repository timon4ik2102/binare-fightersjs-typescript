import { createElement } from '../helpers/domHelper';

interface IModelOfFighter {
  _id: string,
  name: string,
  health: number,
  attack: number,
  defense: number,
  source: string
}

export function createFighterPreview(fighter: IModelOfFighter, position: string) {
  if (!fighter) {
    return '';
  }

  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)
  const fighterImage = createFighterImage(fighter);
  if (position == 'right') fighterImage.style.transform = 'scale(-1, 1)';
  fighterElement.append(fighterImage);
  const fighterInformation = createFighterInfo(fighter);
  fighterElement.append(fighterInformation);

  return fighterElement;
}

export function createFighterImage(fighter: IModelOfFighter) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes
  });

  return imgElement;
}

export function createFighterInfo(fighter: IModelOfFighter) {
  const { attack, defense, health, name } = fighter;

  const fighterInformationBlock = createElement({
    tagName: 'ul',
    className: 'fighter-preview___information'
  });

  const fighterName = createFighterInformationElement();
  fighterName.append(`${name}`);

  const fighterAttackSkills = createFighterInformationElement();
  fighterAttackSkills.append(`attack: ${attack}`);

  const fighterDefenseSkills = createFighterInformationElement();
  fighterDefenseSkills.append(`defense: ${defense}`);

  const fighterHealth = createFighterInformationElement();
  fighterHealth.append(`health: ${health}`);

  fighterInformationBlock.append(fighterName);
  fighterInformationBlock.append(fighterAttackSkills);
  fighterInformationBlock.append(fighterDefenseSkills);
  fighterInformationBlock.append(fighterHealth);

  return fighterInformationBlock;
}

const createFighterInformationElement = () =>
  createElement({
    tagName: 'li',
    className: 'fighter-preview___information__text'
  });